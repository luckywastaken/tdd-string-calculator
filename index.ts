export function stringCalculator(evaluatee: string): number {
    if (evaluatee === '') {
        return 0;
    }

    const parsedNumbers = prepareNumbers(evaluatee);
    return calculateNumbers(parsedNumbers);
}

function prepareNumbers(evaluatee: string): Array<number> {
    const maybeNumbers = evaluatee.split(/[\n,]/);
    const parsedNumbers = maybeNumbers.map(number => parseInt(number));

    checkForNegatives(parsedNumbers);

    return parsedNumbers;
}

function checkForNegatives(numbers: Array<number>) {
    const negativeNumbers = numbers.filter(number => number < 0);

    if (negativeNumbers.length) {
        throwNegatives(negativeNumbers);
    }
}

function throwNegatives(negativeNumbers: Array<number>): never {
    const concatinatedNegatives = negativeNumbers.join(',');

    throw `negatives not allowed: ${concatinatedNegatives}`;
}

function calculateNumbers(numbers: Array<number>): number {
    return numbers
        .filter(number => number < 1000)
        .reduce((sum, number) => sum + number, 0);
}