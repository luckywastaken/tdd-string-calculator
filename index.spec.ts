import {stringCalculator} from "."

describe('String calclulator', () => {
    it('An empty string returns zero', () => {
        const result = stringCalculator('');

        expect(result).toBe(0);
    });

    it('A single number returns the value', () => {
        const result = stringCalculator('4');

        expect(result).toBe(4);
    });

    describe('Two numbers, comma delimited, returns the sum', () => {
        it('For small numbers', () => {
            const result = stringCalculator('2,3');

            expect(result).toBe(5);
        });

        it('For bigger numbers', () => {
            const result = stringCalculator('20,30');

            expect(result).toBe(50);
        })
    });

    it('Two numbers, newline delimited, returns the sum', () => {
        const result = stringCalculator('1\n2');

        expect(result).toBe(3);
    });

    it('Three numbers, delimited either way, returns the sum', () => {
        const result = stringCalculator('1\n2,3\n4');

        expect(result).toBe(10);
    });

    it('Negative numbers throw an exception with the message', () => {
        const thrower = () => stringCalculator('-1,2,-3');

        expect(thrower).toThrow('negatives not allowed: -1,-3');
    });

    it('Numbers greater than 1000 are ignored', () => {
        const result = stringCalculator('1,1000,5');

        expect(result).toBe(6);
    })
})